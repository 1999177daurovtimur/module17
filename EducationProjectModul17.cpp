﻿#include <iostream>

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector() : x(5), y(10), z(15)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		double VectorLenght = 0;
		VectorLenght = sqrt(x * x + y * y + z * z);
		std::cout << VectorLenght;
	}

};

int main()
{
	Vector v;
	v.Show();
}